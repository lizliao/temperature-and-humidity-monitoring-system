import RPi.GPIO as GPIO
import MySQLdb

from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
from time import sleep
from datetime import date, datetime
from sense_hat import SenseHat
sense = SenseHat()
sense.clear()
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()

myConn = MySQLdb.connect("rds-mysql.*****.us-****.rds.amazonaws.com", "admin", "pw", "team")
#input your AWS RDS endpoint, username, password, instance name in order

print myConn

print("myConn Established!")

myMQTTClient = AWSIoTMQTTClient("123afhlss456")
myMQTTClient.configureEndpoint("****.iot.us-west-2.amazonaws.com", 8883)
#input your certificate detail

myMQTTClient.configureCredentials("/home/pi/Desktop/cert/VeriSign-Class 3-Public-Primary-Certification-Authority-G5.pem", "/home/pi/Desktop/cert/Berry.private.key", "/home/pi/Desktop/cert/7ad3371d21-certificate.pem.crt")
#change the location of your certificate
myMQTTClient.configureOfflinePublishQueueing(-1)
myMQTTClient.configureDrainingFrequency(2)
myMQTTClient.configureConnectDisconnectTimeout(30)
myMQTTClient.configureMQTTOperationTimeout(900)
myMQTTClient.connect()
myMQTTClient.publish("Berry","connect",0)



B_cur = myConn.cursor()

user = raw_input("Enter user ID: ")
sql = "SELECT * FROM mydb.User WHERE user_id = %s"
query = sql,(user)
sql = "SELECT * FROM mydb.User WHERE user_id = '{0}'"
query = sql.format(str(user))
result = B_cur.execute(query)
row = B_cur.rowcount   

while row != 1:
    print "Error: %d user has found" % (row)
    user = raw_input("Enter user ID: ")
    
    sql = "SELECT * FROM mydb.User WHERE user_id = %s"
    query = sql,(user)
    sql = "SELECT * FROM mydb.User WHERE user_id = '{0}'"
    query = sql.format(str(user))
    result = B_cur.execute(query)
    row = B_cur.rowcount   
    
    
    
else:
    
    query = sql.format(str(user))
    result = B_cur.execute(query)
    row = B_cur.rowcount
    print "%d user has found" % (row)



plot = raw_input("Enter plot ID(enter 0 to view plot ID list): ")


while plot is "0":
    
    list = B_cur.execute("SELECT plot_id FROM mydb.Plot")
    re = B_cur.fetchall()
    print re
    plot = raw_input("Enter plot ID(enter 0 to view plot ID list): ")

else:

    sql = "SELECT * FROM mydb.Plot WHERE plot_id= %s"
    query = sql,(plot)

    sql = "SELECT plot_id FROM mydb.Plot WHERE plot_id = '{0}'"

    query = sql.format(str(plot))
    result = B_cur.execute(query)
    row = B_cur.rowcount
    

    while row !=1:
            print "Error: %d plot has found" % (row)
            plot = raw_input("Enter plot ID(enter 0 to view plot ID list): ")
            
            while plot is "0":
            
                list = B_cur.execute("SELECT plot_id FROM mydb.Plot")
                re = B_cur.fetchall()
                print re
                plot = raw_input("Enter plot ID(enter 0 to view plot ID list): ")
            else:
                sql = "SELECT * FROM mydb.Plot WHERE plot_id= %s"
                query = sql,(plot)

                sql = "SELECT plot_id FROM mydb.Plot WHERE plot_id = '{0}'"

                query = sql.format(str(plot))
                result = B_cur.execute(query)
                row = B_cur.rowcount
                
                
    
    else:
                print("Raspberry pi has bound with plot "+plot)
                query = sql.format(str(plot))
                result = B_cur.execute(query)
                row = B_cur.rowcount
                print "%d plot has found" % (row)
                sql = "SELECT plot_id FROM mydb.Plot WHERE plot_id = '{0}'"
                sq = "Insert into mydb.Readings (datetime, temperature,humidity, plot_id, user_id)" + "values(%s,%s,%s,%s,%s)"
                while 1:
                    now = datetime.now()
                    now_str = now.strftime('%Y-%m-%d %H:%M:%S')	
                    temp = sense.get_temperature()
                    hum = sense.get_humidity()
                    print plot
                    if temp!= None:
                                    payload = '{ "timestamp": "' + now_str + '" temperature": '+ str("{0:.2f}".format(temp)) + '" humidity": '+ str("{0:.2f}".format(hum))+  '" plot id":'+plot+'"}'
                                    print (payload)
                                    myMQTTClient.publish("Berry/data", payload, 0)
                                    B_cur.execute(sq,(now_str,str("{0:.2f}".format(temp)),str("{0:.2f}".format(hum)),plot,user))
                                    myConn.commit()                      
                                    sleep(20)
                                    
                    else:
                                    print(".")
                                    myConn.close()
                                    sleep(1)
            
		
