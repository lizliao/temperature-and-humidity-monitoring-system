## Project purpose

This project is about temperature and humidity monitoring system which allows users to view the temperature and humidity readings of designated plants from the sensor via computer and mob. 

The raspberry pi connected with SenseHat sensor is placed among the plants of certain parameter (e.g. one sensor for one plot or one sensor for 5 plants etc). The real-time reading data will be uploading continuously from the sensors to the cloud database and the cloud database will then transmit to the web portal. 




---


## Introduction

Using Raspberry Pi and Sense Hat to detect the real-time temperature and humidity

Data is able to read and publish to AWS MQTT and store in AWS RDS